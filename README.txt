Projet PYTHON : multiple scripts

Python 3.9
Date : 15/03/2021
git intervenant : https://gitlab.com/mbalib/formation-python-210315
Documentation : https://docs.python.org/fr/3/library/index.html

-- Execution --
> python hello.py
	- Afficher le contenu du fichier
> type hello.py

-- Creer une documentation --
	- Dans la console
> import sys
> sys.path.append("")
> from pydoc import help
> import ma_classe
> help(ma_classe)
	- Au format html
> python -m pydoc -w ma_classe

-- Installer Linter --
(pour detection d'erreurs dans le code, verification si code de bonne qualite)
> pip install --upgrade pylint


-- Commandes Git --
git clone https://gitlab.com/formation_soft_railway/formation-python.git
cd formation-python

git config --global user.name "user name"
git config --global user.email "example@email.com"

git add myFile.txt
git commit -am "New file"
git push