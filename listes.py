values = [4, 2, 10, 9, 7, 8, 4, 3, 2, 4, 3, 9, 6, 3, 10, 11 ,21]

# afficher tous les cas où deux valeurs se suivent
# ex : 7 10
list = []
i= 0
# Complexité : O(n)
for v in values:
	if i+1<len(values):
		if ((v+1) == (values[i+1])):
			list.append(v)
	i+=1
print (list)

# afficher les cas pour lesquels les 2 suivantes existent aussi dans la liste
# ex : 2 9 7 8 2 6 9
list2=[]
# Complexité : O(2n²)
for v in values:
	if (v+1 in values) and (v+2 in values):
		list2.append(v)
print (list2)

#Complexité : O(n*log(n))
values.sort()
list3=[]
trou = values[0]-1
print ("values = ", values)
for i in range(1,len(values)):	
	# On detecte la presence de trous entre 2 valeurs consecutives
	if (values[i-1]<values[i]-1):		
		trou = values[i]-1
	# Si il y aune distance de deux entre la valeur actuelle et le dernier trou,
	# il y a trois valeurs qui se suivent
	if values[i]>trou+2 and values[i-2]>values[i-3]:
		list3.append(values[i]-2)
print(list3)