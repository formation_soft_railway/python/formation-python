import datetime as D

# Autres modules
# Matrice, séries de nombres : Numpy
# Tableaux de données : Pandas
# Machine learning : Scikit Learn
# Graphes : MathPlotLib

# ------------------------------------------------------------------------------
# -- Exemples
# ------------------------------------------------------------------------------

## -- heure actuelle en AAAA-MM-JJ HH:MM:SS.uS (microsecondes)
# maintenant = D.datetime.now()
# print ("maintenant = ", maintenant)
## -- Crée la date 2021-04-05 00:00:00
# prochainferie = D.datetime(2021, 4, 5)
# print ("Prochain jour ferie : ", prochainferie)
## -- Intervalle de temps
# troisjours = D.timedelta(days=3)
# jourprecedent = prochainferie - troisjours - troisjours
# print ("6 jours avant le jour ferie : ", jourprecedent)
# print ("Il reste " + str(prochainferie-maintenant) + " avant le prochain 
		# jour ferie")
## -- Renvoie le num du mois (ici 4 pour avril)
# print ("Le prochain jour ferie sera en ", prochainferie.month)
# print("Le prochain jour ferie sera le ", prochainferie.day)

# ------------------------------------------------------------------------------
# -- Reformattage log
# ------------------------------------------------------------------------------

log = """
2021-03-10 11:43:23 Warning info 1
2021-03-12 21:33:23 Warning info 2
2021-03-15 12:43:23 Error info 3
2021-03-17 11:43:23 Warning info 4
2021-03-18 10:43:23 info info 5
2021-03-18 12:00:23 info info 7
"""
# reafficher le log en remplaçant les dates/heures par "moins d'une heure", 
# "aujourd'hui", "hier"
date_complet=D.datetime.now()
aujourdui = D.date.today()

for ligne in log.splitlines():
	year = ""
	month = ""
	day = ""
	if ligne != '':
		# plus court : year = int(ligne[0:4]) etc...
		for i in range(10):
			if i < 4 :
				year += ligne[i]
				
			elif i >4 and i <7:
				month += ligne[i]
				
			elif i > 7 and i<10:
				day += ligne[i]
		year = int(year)
		month = int(month)
		day = int(day)
		datelog = D.date(int(year), int(month), int(day))
	
		if aujourdui == datelog:
			heure = int(ligne[11:13])
			min = int(ligne[14:16])
			sec = int(ligne[17:20])			
			if (date_complet-D.datetime(year, month, day, heure, min, sec) < 
				D.timedelta(hours=1)):
				date_convertie = "Moins d'une heure"
			else :
				date_convertie = "Aujourdui        "
		elif aujourdui-datelog == D.timedelta(days=1):
			date_convertie = "Hier             "
		else:
			date_convertie = "Il y a " + str((aujourdui-datelog).days) + " jours   "
		print (date_convertie + "  " + ligne[20:])


# --- Corrigé ---		
# for ligne in log.splitlines():
    # if ligne=="": continue
    # y = int(ligne[0:4])
    # m = int(ligne[5:7])
    # d = int(ligne[8:10])
    # h = int(ligne[11:13])
    # mi = int(ligne[14:16])
    # s = int(ligne[18:20])
    # t = datetime.datetime(y,m,d,h,mi,s)
    # maintenant = datetime.datetime.now()
    # hier = maintenant - datetime.timedelta(days=1)
    # if maintenant - t < datetime.timedelta(hours=1):
        # ligne = "Moins d'une heure  " + ligne[19:]
    # elif (maintenant.year == t.year and maintenant.month == t.month 
			# and maintenant.day == t.day):
        # ligne = "Aujourd'hui        " + ligne[19:]
    # elif hier.year == t.year and hier.month == t.month and hier.day == t.day:
        # ligne = "Hier               " + ligne[19:]
    # print(ligne)