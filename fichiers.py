
base = "C:\\Users\\vtamimo\\Documents\\Formation Software Engineering for railway\\formation-python"
entree = open(base + "\\logcat.txt", "r")

# ------------------------------------------------------------------------------
# -- Filtrage log
# ------------------------------------------------------------------------------
# Creer logcat25390.txt a partir de logcat.txt
sortie1 = open(base + "\\logcat25390.txt", "w")

ligne = entree.readline()
while ligne != '':
	if ligne[19:24]=="25390":
	#if " 25390 " in ligne:
		sortie1.write(ligne)
	ligne = entree.readline()

# ________________
#	
# --- Corrigé ---	
# ________________

# for l in entree:
    # elements = l.split()
    # if len(elements)>1 and elements[2]=="25390":
        # fw.write(l)

print ("Logs associés à pid = 25390 récupérés")

sortie1.close()
# entree.close()

# ------------------------------------------------------------------------------
# -- Ordonner log
# ------------------------------------------------------------------------------

# Creer logcat_parprocessus.txt à partir de logcat.txt (regroupe par proc. parent)

# entree = open(base + "\\logcat.txt", "r")
entree.seek(0) # retour au debut du fichier

lignes_rangees = {}

ligne = entree.readline()
while ligne != '':
	pid = ligne[19:24]
	if pid in lignes_rangees:
		lignes_rangees[pid].append(ligne)
	else:
		lignes_rangees[pid] = [ligne]
	ligne = entree.readline()

sortie2 = open(base + "\\logcat_parprocessus.txt", "w")
liste_cles = list(lignes_rangees.keys())
liste_cles.sort
for key in liste_cles:
	for ligne in lignes_rangees[key]:
		sortie2.write(ligne)

print("Le fichier est classé par processus parent")

sortie2.close()
entree.close()

# ---- Plusieurs solutions possibles ----
# solution 1 : 
# tout enregistrer en mémoire, organisé (un dictionnaire, une liste...), puis
# écrire, regrouper dans l'ordre
# défaut : fichier de 1Go = mémoire de 1Go (voire +)

# solution 2 : 
# idem, mais dans des fichiers isolés (un par processus)
# défaut : fichier de 10000 pid (processus) = 10000 fichiers créés / ouverts

# solution 3 : 
# relire le fichier une fois par numéro de processus
# défaut : complexité n**1.5

# solution 4 : 
# réaliser une liste des pid et longueur de chaque ligne, puis reconstituer le
# fichier final en se déplaçant au bon endroit dans le fichier source
# défaut : nombreux déplacements dans le fichier source

# ________________
#	
# --- Corrigé ---	
# ________________

# fr = open("logcat.txt", "r")
# liste = []
# pids = set()
# pos = 0
# for ligne in fr:
    # longueur = len(ligne)+1
    # elements = l.split()
    # if len(elements)>1:
        # pid = elements[2]
        # liste.append({ "pos": pos, "pid": elements[2], "longueur":longueur })
        # pids.add(elements[2])
    # pos += longueur
# fr.close()
# fr = open("logcat.txt", "r")
# fw = open("logcat_parprocessus.txt", "w")
# for pid in pids:
    # for ligne in liste:
        # if ligne["pid"]==pid:
            # fr.seek(ligne["pos"])
            # fw.write(fr.readline())
# fw.close()
# fr.close()

# ------------------------------------------------------------------------------
# -- Fichiers binaires
# ------------------------------------------------------------------------------

fich_bin = open(base + "\\.git\\index", "rb")
contenu = fich_bin.read() # de type bytes (collections d'octets)
print ("octet 1 = " + str(contenu[0]))
fich_bin.close()