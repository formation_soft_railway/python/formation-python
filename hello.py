#! /usr/bin/env python
# coding: utf-8

# Un commentaire
print("Hello")
print \
("you"
)
a = None

#chaîne multi-ligne
f = """toto
a
vu
titi"""
print (f)
print ("f vaut : \n ---\n",f)
#g = input("ecrit quelque chose : ")

b=2
try:
	print("ok")
	b = b/0
	print ("ok 2")
#except ZeroDivisionError:
#	print("/0")
except ZeroDivisionError as ex:
	print("Erreur div 0 : ", ex)
except:
	print ("Erreur innatendue")
finally:
	print ("Appel dans tous les cas")
print ("Appel")

try:
	print ("test")
	b = b+2
except :
	print("erreur")
else :
	print ("b =", b)
	
for i in range(-4,-11,-3):
	print(i)
