# >>> f5 = lambda x,y: x*y
# >>> f5(2,3)
# 6
# >>> l1 = [3,4,3,2,1,4]
# >>> l2 = filter(lambda x:x<3, l1)
# >>> list(l2)
# [2, 1]

data = (2.3, 4.0, 6.5, 1.0, 3.4)

#Nombres dont la partie entière est paire
# 2.3, 4.0, 6.5
filtereddata = list(filter(lambda x:int(x)%2 == 0,data))
print(filtereddata)