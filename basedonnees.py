# Acces a une base SQLite 3
import sqlite3 as S
import datetime as D

base = "C:\\Users\\vtamimo\\Documents\\Formation Software Engineering for railway\\formation-python"

# Creation/Ouverture d'une BDD
connect = S.connect(base + "\\log.sqlite")

connect.execute(" CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY,info TEXT)")

# Ajout d'une valeur à la BDD
info = "Il est : " + str(D.datetime.now())
connect.execute("INSERT INTO messages(info) VALUES(?)", (info,))
connect.commit()

# Lecture dans la BDD
cursor = connect.cursor()
cursor.execute("SELECT * FROM messages")
for ligne in cursor.fetchall():
	print (ligne)
cursor.close()

connect.close()