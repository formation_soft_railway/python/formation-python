# Module qui permet l'utilisation des regexp (expressions regulieres/rationnelles)
import re

# Traitement sur les chaines
textes = (
			"Ce texte est un texte normal en français",
			"This is an English written text, written by an English person",
			"My tailor is rich, Ben is in the kitchen,Look, it's a plane, a bird, no it's Superman"
		)

# ------------------------------------------------------------------------------
# -- Linguistique
# ------------------------------------------------------------------------------
		
# differences (info statistique, cf : https://en.wikipedia.org/wiki/Letter_frequency ) :
# anglais : 2% de w, 2% de y, 2.7% de u, 0.1% de q, 6% de h
# français : 0% de w, 0.1% de y, 6% de u, 1.4% de q, 0.6% de h
# => Algo qui dit si chaque texte est probablement français ou anglais
print("--------------\n Linguistique : \n--------------")
stats = {
		'fr' : {'w' : 0, 'y' : 0.1, 'u': 6, 'q' : 1.4, 'h' : 0.6},
		'en' : {'w' : 2, 'y' : 2, 'u': 2.7, 'q' : 0.1, 'h' : 6}
}

# --- Corrigé ---
# lettres_utiles = ('h','q','u','w','y')
# for texte in textes:
    # pourcents = {}
    # for l in lettres_utiles:
        # pourcents[l] = 0
    # for c in texte.lower():
        # if c in lettres_utiles:
            # pourcents[c] += 1/len(texte.replace(' ',''))
    # print(" * "+texte)
    # print(pourcents)
    # if pourcents['h']>pourcents["u"]:
        # print("Anglais")
    # else:
        # print("Français")

for text in textes:
	pourcent_w=text.lower().count('w')/len(text.replace(' ', ''))
	pourcent_h = text.count('h')/len(text)
	pourcent_q = text.count('q')/len(text)
	pourcent_u = text.count('u')/len(text)
	pourcent_y = text.count('y')/len(text)
	
	# print ("-----\nnb_h = " + str(nb_h) + " - nb_q = " + str(nb_q) + " - nb_u = " + str(nb_u) +
		# " - nb_w = " + str(nb_w) + " - nb_y = "+ str(nb_y)  )
	

	if (pourcent_h > pourcent_u):
		print ("Le texte est probablement en anglais")		
	else :
		print ("Le texte est probablement en français")

# ------------------------------------------------------------------------------
# -- Syntaxe
# ------------------------------------------------------------------------------
		
# Indiquer (True/False) si la syntaxe est correcte : une ',' est suivie d'un 
# espace
print("--------------\n Syntaxe : \n--------------")
for texte in textes:
	syntaxe_ok = True
	
	# --- Corrigé ---
	# for i in range(len(texte)-1):
		# if texte[i] ==',' and texte[i+1] != ' ':
			# syntaxe_ok = False
			
	if ',' in texte:
		position = texte.find(',', 0)
		while syntaxe_ok and position>0 and position < len(texte)-1 :
			if texte[position + 1] != ' ':
				syntaxe_ok = False
			position = texte.find(',', position+1)
			
	print (syntaxe_ok)
	
# ------------------------------------------------------------------------------
# -- Sujet
# ------------------------------------------------------------------------------
	
# Indiquer si on trouve "text", "mot" ou "word" dans les textes
print("--------------\n Sujet : \n--------------")
for texte in textes:
	linguistique = "text" in texte or "mot" in texte or "word" in texte
	print(texte + " : "+ str(linguistique))

# ------------------------------------------------------------------------------
# -- Mots simples
# ------------------------------------------------------------------------------
	
# grace aux regexp : indiquer si c'est un texte simple :
# - aucun mot de plus de 9 lettres
# - aucun tiret (-), point-virgule (;), guillemet (")
print ("--------------\n Mots simples : \n--------------")
for texte in textes:

	# --- Corrigé ---
	# simple = re.match(".*[A-Za-z]{10}", texte) == None
    # if simple:
		# simple = re.match(".*[;\"\-]", texte) == None

	list = texte.split()
	simple = True
	for mot in list:
		# .* ==> recherche n'importe où. Sans ça, recherche au debut uniquement
		aux = ((re.match(".*[A-Za-z]{10}", mot) == None) and 
				# re.match(".*[;\-\"]", mot) == None 
				#		==> peut remplacer le code ci-dessous
				re.match(".*\-", mot) == None and
				re.match(".*;", mot) == None and
				re.match(".*\"", mot) == None)
		simple = aux and simple
	print ( texte + " : " + str(simple))