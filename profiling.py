import cProfile # profiler en C
import profile # profiler en Python
import re

delimiter = "\n------------------------\n"
bool=re.match(".*a.*", "abc")

print (delimiter+"Profiling re.match"+delimiter)
# Permet d'obtenir le temps d'exécution de la fonction
cProfile.run('re.match(".*a.*", "abc")')

def slow1():
    a=[]
    for i in range(100000):
        a.append(i)
        
def slow2():
    for i in range(10):
        slow1()

# ______________________________________________________________________________
#
# -- Resultat du profiling en C (CProfile) --
# ______________________________________________________________________________
#
# ncalls : nombre de fois où la fonction est appelée
# tottime : temps total de l'execution de la fonction
# percall (num. 1) : temps pour chaque appel de la fonction
# cumtime : temps passé dans la fonction (avec les appels vers les fonctions a 
#           l'intérieur (dans notre cas, appel à append pour slow1()) total
# percall (num.2) : temps passé dans la fonction (avec les appels vers les  
#                   fonctions a l'intérieur (dans notre cas, appel à append pour
#                   slow1()) a chaque appel
# filename : nom du fichier dans lequel se trouve la fonction appelée
# lineno : num. de la ligne dans le fichier
# function : fonction exécutée
# ______________________________________________________________________________
    
print (delimiter+"Profiling slow2 en C"+delimiter)
cProfile.run('slow2()')


#
#         1000014 function calls in 0.236 seconds
#
#   Ordered by: standard name
#
#   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#        1    0.000    0.000    0.236    0.236 <string>:1(<module>)
#       10    0.136    0.014    0.217    0.022 profiling.py:10(slow1)
#        1    0.019    0.019    0.236    0.236 profiling.py:15(slow2)
#        1    0.000    0.000    0.236    0.236 {built-in method builtins.exec}
#  1000000    0.081    0.000    0.081    0.000 {method 'append' of 'list' objects}
#        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}

# ---------
# ==> Si le temps d'exécution de append était 2 fois plus rapide, le temps
#     d'exécution total serait de 0.236-0.81/2
#                              ou 0.136+0.019+0.081/2



# ______________________________________________________________________________
#
# -- Resultat du profiling en Python (profile) --
# ______________________________________________________________________________

print (delimiter+"Profiling slow2 en python"+delimiter)
profile.run('slow2()')


#         1000015 function calls in 1.875 seconds
#
#   Ordered by: standard name
#
#   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
#  1000000    0.984    0.000    0.984    0.000 :0(append)
#        1    0.000    0.000    1.859    1.859 :0(exec)
#        1    0.000    0.000    0.000    0.000 :0(setprofile)
#        1    0.000    0.000    1.859    1.859 <string>:1(<module>)
#        0    0.000             0.000          profile:0(profiler)
#        1    0.016    0.016    1.875    1.875 profile:0(slow2())
#       10    0.875    0.087    1.859    0.186 profiling.py:12(slow1)
#        1    0.000    0.000    1.859    1.859 profiling.py:17(slow2)

# ==> Temps d'excecution fortement augmenté car le profiler de Python est moins 
#     performant que celui en C. Son utilisation ne sera pas forcément pertinent 