# Tests unitaires
import re

# ------------------------------------------------------------------------------
# -- Classe Correction
# ------------------------------------------------------------------------------
class Correction:

	# self n'est pas nécessaire ici car pas besoin de créer un objet pour 
	# executer la fonction
	def mettre_en_minuscule(self, s):
		# Lorsque doctest.testmod() est excéxuté, le code avec ">>>" est exécuté
		# On ecrit le code qu'on ecrirait dans une console en commençant par ">>>"
		# et ce qu'on attend en sortie
		# Le résultat sera comparé au résultat indiqué ci-dessous
		# On peut faire plusieurs tests à la fois
		"""Met en minuscules uniquement la chaine
		
		>>> c = Correction()
		>>> c.mettre_en_minuscule("Hello !")
		'hello !'
		>>> s = c.mettre_en_minuscule("ABC")
		>>> s[0]
		'a'
		>>> s[1]
		'b'
		"""
		# L'erreur est renvoyée à l'exécution dans la console
		# ____________________________________________________
		#**********************************************************************
		# File "C:\Users\vtamimo\Documents\Formation Software Engineering for railway\formation-python\tests.py", line 18, in __main__.Correction.mettre_en_minuscule
		# Failed example:
			# c.mettre_en_minuscule("Hello !")
		# Expected:
			# 'hello !'
		# Got:
			# 'HELLO !'
		# **********************************************************************
		# 1 items had failures:
		   # 1 of   2 in __main__.Correction.mettre_en_minuscule
		# ***Test Failed*** 1 failures.
		# ____________________________________________________
		#
		# return s.upper()
		return s.lower()
		
	# Remplace les blocs d'au moins un chiffre par ***
	def enlever_telephones (self, s):
		""" Remplace les numéros de tel par des étoiles (i.e. :)
		
		>>> c = Correction()
		>>> c.enlever_telephones("0654875956")
		'***'
		>>> c.enlever_telephones("**45**89")
		'**********'
		>>> c.enlever_telephones("hello")
		'hello'
		>>> c.enlever_telephones("")
		''
		"""
		return re.sub("[0-9]+", "***", s) # Utilisation de regexp

	# Dans le cas d'une fonction sans return, on teste les objets modifiées par 
	# la fonction
	def creer_attribut(self):
		"""Test dans le cas d'une fonction sans return
		
		>>> c = Correction()
		>>> c.creer_attribut()
		>>> c.attribut
		29
		"""
		self.attribut = 29


# ------------------------------------------------------------------------------
# -- Main
# ------------------------------------------------------------------------------
# On n'exécute pas si le fichier est importé 
if __name__ == "__main__":
	import doctest
	doctest.testmod()
	print("Tests finis !")