# Permet de repondre a la req PEP257 : docstrings => Corrige l'erreur :
# C0116: Missing function or method docstring (missing-function-docstring)
# renvoye a l'execution de pylint
""" Module de test des multiples classes """

# -----------------------------------------------------------------------
# -- Classe Capteur
# -----------------------------------------------------------------------

# Creer une classe capteur avec un nom, une valeur min, actuelle et max
class Capteur:
    """ Exemple de classe complete
    Avec constructeur, methode et meme surcharge d'operateur
    """
    # Inutile si on a un constructeur
    # nom = ""
    # min = 0
    # max = 100
    # valeur = 0

    # Constructeur - appele a chaque creation d'objet
    def __init__(self, nom = "", min = 0, valeur = 0, max = 100):
        """ Constructeur
        self : objet en cours
        nom : nom du capteur
        """
        self.nom = nom
        self.min = min
        self.max = max
        self.valeur = valeur

    # permettre Capteur*3 et Capteur/3
    # (modifier tous les attributs) - "mul" et "truediv"
    def __mul__(self, x):
        return Capteur(self.nom, self.min*x, self.valeur*x, self.max*x)

    def __truediv__(self, x):
        #return Capteur(self.nom, self.min/x, self.valeur/x, self.max/x)
        # Utilisation de la surcharge de * faite precedemment
        return self * (1/x)

    # Creer une methode qui affiche le capteur sous la forme d'une jauge à 10
    # caracteres de la facon suivante : "capteur 8 |#######---|
    def afficher(self):
        str = self.nom + " |"
        for n in range(10):
            if n < (self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            else:
                str += "-"
        str += "|"
        print(str)

# -----------------------------------------------------------------------
# -- Classe DoubleCapteur
# -----------------------------------------------------------------------

#Ajouter classe "DoubleCapteur" avec valeur2 >= valeur
class DoubleCapteur(Capteur):
    """ Classe fille """
    def __init__(self, nom = "", min = 0, valeur = 0, valeur2 = 1, max = 100):
        Capteur.__init__(self, nom, min, valeur, max)
        self.valeur2 = valeur2

    def __mul__(self, x):
        return DoubleCapteur(self.nom, self.min*x, self.valeur*x, self.valeur2*x,
                             self.max*x)

    # Inutile de surcharger truediv car d'apres l'implementation dans Capteur,
    # lorsque truediv est appele, c'est le * de DoubleCapteur qui est appele
    # def __truediv__(self, x):
        # return self * (1/x)

    def afficher(self):
        str = self.nom + " |"
        for n in range(10):
            if n < (self.valeur-self.min) * 10 / (self.max-self.min):
                str += "#"
            elif n < (self.valeur2-self.min) * 10 / (self.max-self.min):
                str += "o"
            else:
                str += "-"
        str += "|"
        print(str)


# -----------------------------------------------------------------------
# -- "Main"
# -----------------------------------------------------------------------
# Lorsque le fichier est importé dans un autre module, __name__ contient le nom 
# du fichier actuel, si le fichier est executé de lui-même (avec la commande 
# python class_x.py dans une console), __name__ contient __main__

# Le code ci-dessous ne sera executé que si on execute le module directement
# Lorsqu'on générera la doc, le code ne sera pas exécuté avant
if __name__ =="__main__":

	capteur8 = Capteur("Capteur 8")
	capteur8.valeur = 66
	capteur8.afficher()

	capteur1 = Capteur("Capteur 1", -5, 2, 5)
	capteur1.afficher()

	capteur4 = capteur1 * 3
	capteur4.afficher()

	capteur2 = capteur8 / 2
	capteur2.afficher()

	capteur3 = DoubleCapteur("Capteur 3", 2, 6, 8, 10)
	capteur3.afficher()

	capteur5 = capteur3 * 2
	capteur6 = capteur3/4
