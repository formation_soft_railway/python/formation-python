"""Fenetre TK"""
import tkinter # Import de Tool Kit Interface

# Crée une fenêtre avec un bouton et une zone de texte
class ModificationFrame(tkinter.Frame):
	def __init__(self, master):
	
		# width et height sont optionnels
		tkinter.Frame.__init__(self, master = master, width=500, height=500)
		self.pack()
		
		# Zone de texte
		self.entry1 = tkinter.Entry(self, text = "Entrer ici")
		self.entry1.pack()
		self.entry1.place(x=10, y=10, width=100, height=50) # placement absolu
		
		# Exemple de lecture
		# texte = entry1.get()
		# Exemple d'écriture
		# entry1.delete(0,"end")
		# entry1.insert(0, "coucou")
		
		# Bouton
		self.bouton2 = tkinter.Button(self, text = "Minuscule", command=self.bouton1_click) 
		# L'appui sur le bouton declenche bouton1_click()
		self.bouton2.pack()
		self.bouton2.place(x=10, y=70, width=100, height=50) # Placement absolu
		
		self.mainloop()
		
	def bouton1_click(self):
		texte = self.entry1.get()
		self.entry1.delete(0,"end")
		self.entry1.insert(0, texte.lower())
		print("Texte mis en minuscule")

if __name__ == "__main__":
	afficheur = tkinter.Tk()
	fenetre = ModificationFrame(afficheur)

# --- Remplacé par la creation de la classe
# fenetre = tkinter.Frame(master = afficheur)
# fenetre.pack()
# bouton1 = tkinter.Button(fenetre, text = "Appuyer ici")
# bouton1.pack()
# bouton2 = tkinter.Button(fenetre, text = "Appuyer là")
# bouton2.pack()
# fenetre.mainloop()
