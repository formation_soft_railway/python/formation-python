# Application du Design Pattern GoF "Composite"
# Organisation arborescente avec des elements qui sont des branches (ici 
# GroupeDeSondes) et des elements qui sont des feuilles (ici SondeSimple)

# -----------------------------------------------------------------------
# -- Classe Sonde
# -----------------------------------------------------------------------

class Sonde:
	# pass # Aucune action n'est faite dans la classe

	def nb_sondes(self):
		return 1
		
	def nb_sondes_simples(self):
		return 1
		
	def v_moyenne(self):
		return self.somme()/self.nb_sondes_simples()
	
# -----------------------------------------------------------------------
# -- Classe SondeSimple
# -----------------------------------------------------------------------	
		
class SondeSimple (Sonde):

	def __init__(self, v = 0):
		self.v = v		
		
	def afficher (self):
		print(self.v)
		
	def v_max(self):
		return self.v
		
	# Cette solution fonctionne aussi
	# def nb_sondes(self):
		# return 1
	
	def somme(self):
		return self.v
			
# -----------------------------------------------------------------------
# -- Classe GroupeDeSondes
# -----------------------------------------------------------------------
			
# Le fait que SondeSimple et GroupeDeSondes heritent de Sonde va permettre de 
# mettre SondeSimple et GroupeDeSondes dans une meme liste		
class GroupeDeSondes (Sonde):

	def __init__(self, sondes):
		self.sondes = sondes # sondes est une liste de Sonde
		
	# la fonction est recursive
	# lorsque s est une SondeSimple, on appelle afficher() de la classe 
	# SondeSimple, lorsque s est une GroupeDeSondes, on appelle afficher() de la
	# classe GroupeDeSondes
	def afficher (self):
		for s in self.sondes:
			s.afficher()
	
	# Valeur max du groupe de sondes
	def v_max(self):
		#max = self.sondes[0].v_max()
		max = -1e10
		# L'algo n'est pas optimal en temps car 2 appels à v_max()
		for s in self.sondes:
			if s.v_max()>max:
				max = s.v_max()
		return max
	
	# Nombre de sondes + groupe de sondes
	def nb_sondes(self):
		nb = 1
		for s in self.sondes: # Parcours de "l'arbre"
			nb += s.nb_sondes()
		return nb
		
	def nb_sondes_simples(self):
		nb = 0
		for s in self.sondes: # Parcours de "l'arbre"
			nb += s.nb_sondes_simples()
		return nb	
		
	def somme(self):
		somme = 0
		for s in self.sondes:
			somme += s.somme()
		return somme
	
	
# -----------------------------------------------------------------------
# -- "Main"
# -----------------------------------------------------------------------
	
s1 = SondeSimple(18)
s2 = SondeSimple(11)
s3 = SondeSimple(8)

gs1 = GroupeDeSondes([s1, s2])
gs1.afficher()

gs2 = GroupeDeSondes ([s3, gs1])
gs2.afficher()

print("Valeur max de s1 :", s1.v_max())
print("Valeur max de gs2 :", gs2.v_max())

print("Nombre de sondes dans gs2 :", gs2.nb_sondes())

print("Moyenne des valeurs de gs2", gs2.v_moyenne())