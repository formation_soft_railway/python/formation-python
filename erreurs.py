try:
	n = int(input("Code ? "))
	print("Valeur rentree : ", n)

	# exemple :
	# 1 => erreurs 1
	# 2 => erreurs 2
	# 4 => erreurs 3
	# 8 => erreurs 4
	# 5 => erreurs 1 et 3
	# 10 => erreurs 2 et 4
	# 7 => erreurs 1 et 2 et 3
	
	# Solution 1
#	for i in range(64):
#		mask = n & 2**i
#		if mask!=0:
#			print ("erreur levee : ", i+1)

	# Autre solution
	mask = 0
	bit = 1
	erreur = ""
	aux_erreur = ""
	while mask < n:
		mask = 2**(bit-1)
		if mask & n != 0:
			erreur += aux_erreur + str(bit)
			aux_erreur = " et "
		bit += 1
	print ("erreurs : ", erreur)
	
except ValueError as ex:
	print ("Vous n'avez pas rentre un entier !")
	print ("Erreur recue : ", ex)
	