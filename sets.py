# -----------------------------------------------------------------------
#							Ensembles
# -----------------------------------------------------------------------

# Algebre ensembliste :
# >>> { (1,), (1,) }
# {(1,)}
# >>> {1,2} & {1,3} => elements communs
# {1}
# >>> {1,2} | {1,3} => tous les elements
# {1, 2, 3}
# >>> {1,2} - {1,3} => soustration d'ensemble
# {2}
# >>> {1,2} ^ {1,3} => elements differents
# {2, 3}

capteur1 = {"fleche", "bord", "arrete"}
capteur2 = {"fleche", "entree"}
capteur3 = {"sortie 2", "entree", "fleche"}

signaux = [capteur1, capteur2, capteur3]

print("Signaux actifs sur tous les capteurs :")
# fleche
capt1 = signaux[0]
for i in range(1, len(signaux)):
	capt1 = capt1 & signaux[i]
print(capt1)

print ("Signaux actifs sur au moins un capteur :")
# fleche, bord, arrete, entree, sortie2
capt2 = signaux[0]
for i in range(1,len(signaux)):
	capt2 = capt2 | signaux[i]
print(capt2)

print("Signaux actifs sur un nombre impair de capteurs :")
# fleche, bord, arrete, sortie 2
capt3 = signaux[0]
for i in range(1, len(signaux)):
	capt3 = capt3 ^ signaux[i]
print(capt3)

# entree
print("Signaux actifs sur un nombre pair de capteurs :")
capt4 = capt2-capt3
print(capt4)

# -----------------------------------------------------------------------
#							Dictionnaires
# -----------------------------------------------------------------------

print("Nombre de fois qu'apparait chaque signal : ")
# entree : 2, fleche : 3, ...
nbSig = {}
for capteur in signaux:
	for signal in capteur:
		if not(signal in nbSig):
			nbSig[signal]=1
		else:
			nbSig[signal]+=1
print(nbSig)

# -----------------------------------------------------------------------
#							Collections
# -----------------------------------------------------------------------

tout = {
	"signaux" : signaux,
	"resultats": {
		"sur tous": capt1,
		"sur au moins un": capt2,
		"sur impair": capt3,
		"sur pair": capt4,
		"occurences":nbSig
	}
}
print(tout)
print ("Occurences de \"entree\":", tout["resultats"]["occurences"]["entree"])